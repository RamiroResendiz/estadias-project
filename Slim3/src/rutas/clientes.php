<?php

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;


//GET Todos los clientes
$app->get('/api/clientes', function(Request $request, Response $response){

    $sql = "SELECT * FROM clientes";

    try {
        $db = new db();
        $db = $db->conecctionDB();
        $resultado = $db->query($sql);

        if($resultado->rowCount() > 0){

        $response->getBody()->write(json_encode($resultado->fetchAll(PDO::FETCH_OBJ)));
        return $response
            -> withHeader('Content-Type','aplication/json')
            -> withStatus(200);
            
        }else{
            echo json_encode("No existen clientes en la BBDD");
        }
        $resultado = null;
        $db = null;
    } catch (PDOException $e) {
        echo '{"error" : {"text" : '.$e->getMessage().'}';
    }
});




//GET Clientes por id
$app->get('/api/clientes/{id}', function(Request $request, Response $response){
    $id_cliente = $request->getAttribute('id');
    $sql = "SELECT * FROM clientes WHERE id = $id_cliente";

    try {
        $db = new db();
        $db = $db->conecctionDB();
        $resultado = $db->query($sql);

        if($resultado->rowCount() > 0){

        $response->getBody()->write(json_encode($resultado->fetchAll(PDO::FETCH_OBJ)));
        return $response
            -> withHeader('Content-Type','aplication/json')
            -> withStatus(200);
            
        }else{
            echo json_encode("No existe cliente con ese ID en la BBDD");
        }
        $resultado = null;
        $db = null;
    } catch (PDOException $e) {
        echo '{"error" : {"text" : '.$e->getMessage().'}';
    }
});


//POST Crear nuevo cliente
$app->post('/api/clientes/nuevo', function(Request $request, Response $response){
    $nombre = $request->getParam('nombre');
    $apellido = $request->getParam('apellido');
 

    $sql = "INSERT INTO clientes (nombre, apellido) VALUES 
            (:nombre, :apellido)";

    try {
        $db = new db();
        $db = $db->conecctionDB();
        $resultado = $db->prepare($sql);

        $resultado->bindParam(':nombre', $nombre);
        $resultado->bindParam(':apellido', $apellido);
   

        $resultado->execute();

        echo json_encode("Nuevo cliente guardado.");

        $resultado = null;
        $db = null;
    } catch (PDOException $e) {
        echo '{"error" : {"text" : '.$e->getMessage().'}';
    }
});


//PUT Modificar cliente
$app->put('/api/clientes/modificar/{id}', function(Request $request, Response $response){
    $id_cliente = $request->getAttribute('id');
    $nombre = $request->getParam('nombre');
    $apellido = $request->getParam('apellido');
  

    $sql = "UPDATE clientes SET 
                nombre = :nombre,
                apellido = :apellido,
            WHERE id = $id_cliente";



    try {
        $db = new db();
        $db = $db->conecctionDB();
        $resultado = $db->prepare($sql);

        $resultado->bindParam(':nombre', $nombre);
        $resultado->bindParam(':apellido', $apellido);


        $resultado->execute();

        if($resultado->rowCount()>0){
            echo json_encode("Cliente Modificado.");
        } else {
            echo json_encode("No existe cliente con ese ID.");
        }        


        

        $resultado = null;
        $db = null;
    } catch (PDOException $e) {
        echo '{"error" : {"text" : '.$e->getMessage().'}';
    }
});


//DELETE Eliminar cliente
$app->delete('/api/clientes/delete/{id}', function(Request $request, Response $response){
    $id_cliente = $request->getAttribute('id');
    $sql = "DELETE FROM clientes WHERE id = $id_cliente";

    try {
        $db = new db();
        $db = $db->conecctionDB();
        $resultado = $db->prepare($sql);
        $resultado->execute();

        if($resultado->rowCount()>0){
            echo json_encode("Cliente eliminado.");
        } else {
            echo json_encode("No existe cliente con ese ID.");
        }        

        $resultado = null;
        $db = null;
    } catch (PDOException $e) {
        echo '{"error" : {"text" : '.$e->getMessage().'}';
    }
});